# Header
## Category 1
### Sub Category 1.1
Blabla bla `code` **bold** *italic* __underline__ ~~France~~

> Quote

### Sub Category 1.2

This script shows this ![ImageMSG](img/Msg2.png)
![Test|center](img/Msg2.png "Figure 1 : Message test")

Here's the script : 

```ruby
def a
  print "Wololo"
end
```
## Category 2

1. First thing
2. Second thing
3. Third thing

* We don't care  
    Test
* Wololo
    * Bis

## Category 3
Wololo. [^1]
Lot of things..

### Sub Category 3.1

| Test1 | Test2 |
|-------+-------|
| 0.25  | 0.13  |
| 45    | 11    |

#### Sub Category 3.1.1

```ruby
class MyClass < Parent
  CONST = 5
  CONST2 = "6#{:lol}"
  CONST3 = '3'
  CONST5 = [ { a: 0 } ]
  def initialize(viewport, wololo = 5, key: 3)
    super(viewport)
    @wololo = wololo
    @key = key
  end
end
```

[^1]: Test Footnote