# Editeur de Quêtes

![tip](warning "Cette interface est relativement peu pratique et pourrait être amenée à changer")

## Interface

L'interface des quêtes se présente ainsi :
![Interface|center](img/ruby_host/quest.png "Interface d'édition des quêtes")

Elle contient ce qu'il faut pour ajouter les objectifs d'une quête ainsi que ses gains.

### Choix d'un type d'objectif

Le menu déroulant `Type d'objectif` propose les objectifs suivants :
- Parler à un PNJ : remplir le champ `Nom :`
- Battre un PNJ : remplir le champ `Nom :`
- Trouver objet : choisir l'objet dans le champ `Objet :` et remplir le champ `Quantité :`
- Voir Pokémon : remplir le champ `Quantité :` et choisir le Pokémon dans le champ `Pokémon :`
- Battre Pokémon : Comme Voir Pokémon
- Capturer Pokémon : Comme Voir Pokémon
- Obtenir oeuf : Remplir le champ `Quantité :`
- Eclore oeuf : Remplir le champ `Quantité :`

Une fois fait, vous pouvez ajouter l'objectif en cliquant sur `Ajouter`. (Vous ne décidez malheureusement pas de l'ordre dans cette interface.)

### Supprimer un objectif

Pour supprimer un objectif choisissez l'objectif que vous voulez supprimer dans la liste d'objectifs puis cliquez sur `Supprimer`.

### Choix des récompenses

Le menu déroulant `Type de récompense` propose deux type de récompenses :
- `Argent` : Remplir le champ `Quantité :`
- `Objet` : choisir l'objet dans le champ `Objet :` et remplir le champ `Quantité :`

Vous pouvez ajouter plusieurs de ces récompenses dans une quête. La suppression se fait comme pour les objectifs mais avec les boutons sous la liste des récompenses.
