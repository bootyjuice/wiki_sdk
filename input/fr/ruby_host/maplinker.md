# Le Map Linker

Le Map Linker est l'outil de PSDK pour lier des maps entre elles. A l'heure actuelle, il n'est possible de lier que 4 maps entre elles : Nord, Sud, Est, Ouest.

## Comment utiliser le Map Linker ?

Voici une image démontrant comment sont liés des maps :
![Demo MapLinker](img/ruby_host/maplinker_tutorial.png "Démo MapLinker")

Sur cette image il y a trois maps qui sont liés. Les calculs d'offset vers la droite ou le bas sont fait ent comparant les trois colonnes ou ligne commune entre la map actuellement éditée et celle qui luis sont liés.
- Si la flèche vas vers la direction d'offset on met le nombre de tiles (positif) pour réaliser l'offset.
- Si la flèche vas vers la direction opposé d'offset, on met le nombre opposé de tiles (négatif)
- Si vous ne pouvez pas dessiner de flèche, mettez 0.

![tip](info "L'éditeur du Map Linker réalise le lien automatiquement entre les deux maps, il n'est donc pas nécessaire de repasser sur la map liée pour la lier à celle que vous avez édité")

![tip](warning "Pour que la téléportation fonctionne bien, vous devez bien faire attention à laisser **3** ligne/collone en commun sur les bordure des maps liés dans la direction du lien. Comme sur la capture d'écran.")