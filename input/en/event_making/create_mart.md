# Open a Shop

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Open a item shop

You have two solutions to open an item shop.
- Using the RMXP command.
- Using the following script command :
```ruby
$game_temp.shop_calling = true
$game_temp.shop_goods = [
  [0, id_item1],
  [0, id_item2],
  # etc...
]
```

![tip](info "It's recommanded to add a wait command after this script command to let the interpreter synchronize itself with the map.")

## Open a Pokemon shop

To open a Pokemon shop you need to use the command pokemon_shop_open. This command takes three parameters :

- An array of Pokemon ID
- An array of Price related
- An array of level that can also be Hash describing the Pokemon.

Example : A shop selling a Pikachu level 50 (2500$) and an Alolan Meowth level 15 (500$) :
```ruby
pokemon_shop_open([25, 52], [2500, 500], [50, { id: 52, level: 15, form: 1 }])
```