# Event Making

This section of the Wiki contain all the tutorials related to Event Making under Pokémon SDK.

Some basic information can be presented here.

You'll find how to :
- [Show a message](messages.md)
- [Show a choice](choices.md)
- [Give a Pokemon](give_pokemon.md)
- [Give an item](give_item.md)
- [Enable the Pokedex](enable_pokedex.md)
- [Give a badge](give_badge.md)
- [Create a Pokémon Center](create_pokemon_center.md)
- [Open a Shop](create_mart.md)
- [Start a wild battle](start_wild_battle.md)
- [Create a Trainer event](trainer_event.md)
- [Release a roaming Pokémon](roaming_pokemon.md)
- [Manage quests](quest.md)
- [How to use the FollowMe system](followme.md)
- [Edit Event attributes](event_attribute.md)
- [Manage Time system](time-system.md)