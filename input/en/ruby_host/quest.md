# Quests editor

![tip](warning "This editor is not really practical and will probably change in the future")

## The interface

This is how the Quest editor looks like :
![Interface|center](img/ruby_host/quest.png "Quest editor")

It contain everything you need to set the quest goal & earnings.

### Choice of an new goal

In the `Objective type:` dropdown you have the following choice :
- Speak to an NPC : fill the `Name :` field
- Beat an NPC : fill the `Name :` field
- Find an Item : choose the item in `Item :` and fill the `Amount :` field
- Find a Pokémon : fill the `Amount :` field and choose the Pokémon in `Pokémon :`
- Beat a Pokémon : same
- Capture a Pokémon : same
- Obtenir oeuf : fill the `Amount :` field
- Eclore oeuf : fill the `Amount :` field

Once the goal is configured, click on `Add`. (You can't choose the order.)

### Remove a goal

Choose the goal you want to remove in the list of goals an click on the `Remove` button under the list.

### Earning choice

The `Earning type` dropdown allow you to choose two kind of earning :
- `Money` : fill the `Amount :` field
- `Item` : choose the item in `Item :` and fill the `Amount :` field

You can choose multiple earning and the removal works the same as the goals but under the earning list.