# Trainer editor

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Trainer editor in Ruby Host
The `Trainers` button from the Home UI allows you to edit the trainer
![Interface|center](img/ruby_host/Dresseur.png "Trainer editor UI")

## Create a new trainer
To create a new trainer click on `add` under the `Trainer` list. You can define :

### The trainer name

In the `Name or names` field you can input the name or the list of trainer name (if it's 2v2 battle).

### The given money

In the `Base money` field you can edit the factor multiplied to the last's Pokémon level.

### Trainer speech

Cick on `Edit victory/defeat speech`. It shows the text editor (with the right text id), you can edit the speech there.

### Double battle mode

Check the `Double battle` checkbox.
![tip](warning "You should give two names with this option!")

### Its battler

Double click on the battler image (under `Battler:`)
![tip](info "The battler should come from the Graphics/Battlers folder of the project!")
![tip](info "If you can't put another battler, re-open the project with Ruby Host, the **path** is probably corrupted.")

### Its special group

This option will be deprecated in Alpha 25.

### Its Pokémon

You can add/remove various Pokémon from the Party list. Click on a Pokémon to edit its properties (EV, IV etc...)

### Special case

The `Trainer team n°:` allows you to tell which trainer own the Pokémon in 2v2 (0 = first, 1 = second)

The form correspond to the form of the Pokemon, for example 1 = Unkown B if you're editing the Unkown's properties.

Once done, click on `Save` and go back to RMXP in order to edit the [Trainer event](fr/event_making/trainer_event.md).
