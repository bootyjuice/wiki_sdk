class WikiGenerator
  class GenericArticleFooter < GenericArticleHeader
    private

    # Wrap each part of the article header
    # @param previous [String] previous article
    # @param article_selector [String] article selector
    # @param next_art [String] next article
    # @param lang_selector [String] language selector
    # @return [String]
    def wrap(previous, article_selector, next_art, lang_selector)
      <<~WRAPPER.gsub('class="dropdown"', 'class="dropdown on_top"')
        <nav class="article_footer_nav">
          #{previous} #{article_selector} #{next_art}
        </nav>
      WRAPPER
    end
  end
end
