class WikiGenerator
  class ArticleHeaderNone < GenericArticleHeader
    EMPTY_RESULT = '<nav class="article_header_nav"></nav>'
    # Process the header for a specific page
    # @param index [Integer]
    # @return [String] the article header
    def process(index)
      EMPTY_RESULT
    end
  end
end
